import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../_services/authentication.service';
import { first } from 'rxjs/operators';
import {AlertService} from '../_services/alert.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  submitted=false;
  success=false;
  returnUrl:string;
  error='';
  constructor(
    private formBuilder:FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService:AlertService) { }

  ngOnInit() {
    this.loginForm=this.formBuilder.group({
      email:['',Validators.required],
      password:['',Validators.required]
    });
    console.log('logout()');
    this.authenticationService.logout();
    this.returnUrl=this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  
  get f() { return this.loginForm.controls; }
  
  onSubmit(){
    this.submitted=true;

    if(this.loginForm.invalid){
      return;
    }
    this.authenticationService.login(this.f.email.value,this.f.password.value)
        .pipe(first()).subscribe(
          data=>{
            this.router.navigate([this.returnUrl]);
          },
          error=>{
            this.alertService.error(error);
          }
        );
  }

}
